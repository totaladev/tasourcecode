/*
** This is the master file for describing TA's memory structure. As components are identified, they are eventually moved into respective header files
**
**
*/

/*
We only decoded those data structures that we needed and those didnt include what you are after. But i include a .h file that codifies what we know.
TAdynmemStruct is the main entry point but if you dont know/can find the pointer to it you will probably not be able to do much anyway :).
We have kept this file semi secret to prevent too advanced cheating so please dont spread it further.

//SY_SJ
*/

//TAdynmemStruct ** = 0x511DE8
//(virtual address 0x511DE8 points to a pointer to TAdynmenStruct)
//however, TA4.dll uses pointers to units fetched by the exported functions GetAttacker, GetUnitScriptIndex, and COBaltExtensionInstruction

#pragma once

#include <Windows.h> //need this for mmsystem
#include <mmsystem.h> //added this cause #include <dsound.h> wasn't enough
#include <dsound.h>

#pragma pack(1)

typedef enum _enPacketID : unsigned char
{
	Encypted			= 0x3,
	Compressed			= 0x4,
	ObjectState			= 0xC
}_enPacketID;


typedef enum _enPlayerType : unsigned char
{
	None				= 0x0,
	LocalHuman			= 0x1,
	AI					= 0x2,
	RemoteHuman			= 0x3
}_enPlayerType;

typedef enum _enSharedStates : unsigned char
{
	Metal				= 0x2,
	Energy				= 0x4,
	LOS					= 0x8,
	Mappings			= 0x20,
	Radar				= 0x40
}_enSharedStates;

//looks to contain status bits?
//WantsToCloak: 3 for no, 11 (0xB) for yes
//NOTE:: set this to 0x43 to kill the unit!!!!! (confirmed using arm commander)
typedef enum _UnitStatusFlags : unsigned char
{
	IsVisible			= 0x2,
	IsUncloaking		= 0x3,
	IsCloaking			= 0x11,

	IsSelfDestructing	= 0x43
}_enUnitStatuSFlags;

struct _Point3Struct;
struct _PlayerResourcesStruct;
struct _PlayerStruct;
struct _WeaponDamageProfileStruct;
struct _WeaponUnkStruct;
struct _WeaponStruct;
struct _ExplosionStruct;
struct _DebrisStruct;
struct _Unk1Struct;
struct _FXGafStruct;
struct _DSoundStruct;
struct _TAdynmemStruct;
struct _WreckageInfoStruct;
struct _FeatureStruct;
struct _FeatureDefStruct;
struct _ProjectileStruct;
struct _MapFileStruct;
struct _PlayerInfoStruct;
struct _UnitDefStruct;
struct _IsUnitStruct;
struct _UnitStruct;
struct _Object3doStruct;
struct _PrimitiveInfoStruct;
struct _PrimitiveStruct;
struct _UnitOrdersStruct;
struct _GAFAnimStruct;
struct _RadarPicStruct;
struct _ParticleSystemStruct;
struct _ParticleBaseStruct;
struct _SmokeGraphicsStruct;
struct _AlmostCOBStruct;

//struct _SmokeListNodeStruct;

typedef struct _Point3Struct
	{
	int	x;
	int	y;
	int	z;
	}_Point3Struct;

typedef struct _PlayerResourcesStruct
{
	float fCurrentEnergy;
	float fEnergyProducton;
	float fEnergyExpense;
	float fCurrentMetal;
	float fMetalProduction;
	float fMetalExpense;
	float fMaxEnergyStorage;
	float fMaxMetalStorage;
	double dTotalEnergyProduced;
	double dTotalMetalProduced;
	double dTotalEnergyConsumed;
	double dTotalMetalConsumed;
	double dEnergyWasted;
	double dMetalWasted;
	float fPlayerEnergyStorage;
	float fPlayerMetalStorage;
	float fShareMetalLimit;
	float fShareEnergyLimit;
}_PlayerResourcesStruct;

typedef struct _PlayerStruct
	{
	DWORD					dwPlayerActive;
	DWORD					dwDirectPlayID; //player localness? formerly p_Unknown1.. is this a ptr?
	BYTE					Unknown1[4];
	BYTE					PlayerNum; //NOT the player index. The player who owns it? aka who a multiplayer AI is owned by
	BYTE					Unknown2[7];	// 0x0D
	short					nPing;
	BYTE					Unknown3[6];
	short					nLastMessageTimeStamp;
	BYTE					Unknown4[2];
	BYTE					MultiLoadProgress;
	BYTE					Unknown5[6];
	_PlayerInfoStruct		*p_PlayerInfo;
	char					szName[30];
	char					szSecondName[30];
	_UnitStruct				*p_Units; //0x67
	_UnitStruct				*p_NextUnits; //ptr to the end of the unit array?
	short					nUnitsIndexBegin;
	short					nUnitsIndexEnd;

	_enPlayerType			_PlayerType; //size BYTE.. "controller" in ida database

	DWORD					dwAIConfig;
	DWORD					*p_Squads;
	DWORD					*p_LOSMemory; //"pLOSStruturePtr"
	DWORD					dwLOSWidth; //"lXOffset"
	DWORD					dwLOSHeight; //"lYOffset"
	DWORD					dwLOSBitsNum;

	_PlayerResourcesStruct	_PlayerResources;
	
	BYTE					Unknown6[4];

	DWORD					dwUpdateTime;
	DWORD					dwWinLoseTime;
	DWORD					dwDisplayTimer;

	unsigned short			unKills;
	unsigned short			unLosses;

	BYTE					Unknown7[4];

	unsigned short			unKillsLast;
	unsigned short			unLossesLast;

	BYTE					byAllyFlagArray[10];

	BYTE					Unknown8[0x2D];

	BYTE					byAllyTeam;

	int						lUnitscounter;
	short					nNumUnits;
	unsigned char 			ucPlayerIndex;	// 0x146 - zero based index of the player
	BYTE					Unknown9;	// 0x146
	unsigned char			ucPlayerScoreboard;
	short					nAddPlayerStorage; //what is this
	}_PlayerStruct; //0x14B

typedef struct _WeaponDamageProfileStruct
	{
	char	*pszUnitName;
	short	nDamage;
	char	data1[2]; //looks like always 0
	}_WeaponDamageProfileStruct;

typedef struct _WeaponUnkStruct
	{
	char data1[5]; //looks like a pointer and a char[1]
	_WeaponDamageProfileStruct *pWeaponDamageProfileStructArray;
	//end of array looks like: pszUnitName, nDamage, data1[2] != 0... this of course assumes data1[2] == 0 in all other cases
	}_WeaponUnkStruct;

typedef struct _WeaponStruct
	{
	char				szWeaponName[0x20];
	char				szWeaponDescription[0x40];
	DWORD				*pFireCallback; //formerly unk1

	_WeaponUnkStruct	*p_WeaponUnkStruct;


	int					lWeaponVelocity;
	int					lStartVelocity;
	int					lWeaponAcceleration;
	_DebrisStruct		*p_Debris;

	_GAFAnimStruct		*p_LandExplodeAsGFX;
	_GAFAnimStruct		*p_WaterExplodeAsGFX;

	char				szModelName[0x30];
	BYTE				Unknown1[0x10];

	int					lEnergyPerShot;
	int					lMetalPerShot;
	int					lMinBarrelAngle;
	int					lShakeMagnitude;
	int					lShakeDuration;

	short				nDefaultDamage;
	short				nAreaOfEffect;
	float				fEdgeEffectivnes;
	short				nRange;

	BYTE				Unknown2[2];

	int					lCoverage;
	short				nReloadTime;
	short				nWeaponTimer;
	short				nTurnRate;
	short				nBurst;
	short				nBurstRate;
	short				nSprayAngle;
	short				nDuration;
	short				nRandomDecay;

	short				nSoundEffectID_SoundStart;
	short				nSoundEffectID_SoundHit;
	short				nSoundEffectID_SoundWater;
	
	short				nSmokeDelay;
	short				nFlightTime;
	short				nHoldTime;

	BYTE				Unknown3[4];

	short				nAccuracy;
	short				nTolerance;
	short				nPitchTolerance;

	unsigned char		cID;

	unsigned char		cFireStarter;
	unsigned char		cRenderType;
	unsigned char		cColor;
	unsigned char		cColor2;
	
	BYTE				Unknown4[2];

	DWORD				dwWeaponTypeMask;
	}_WeaponStruct; //0x115

typedef struct _ExplosionStruct
	{
	_DebrisStruct	*p_Debris;
	short			Frame;
	char			data2[6];
	_FXGafStruct	*FXGaf;
	char			data3[12];
	int				XPos;
	int				ZPos;
	int				YPos;
	char			data4[36];
	short			XTurn; //0x4C
	short			ZTurn;
	short			YTurn;
	char			data5[2];
	}_ExplosionStruct; //0x54

typedef struct _DebrisStruct
	{
	char		data1[0x24];
	_Point3Struct		*Vertices;
	_Unk1Struct	*Unk;
	char		data2[8];
	}_DebrisStruct; //0x34

typedef struct _Unk1Struct
	{
	char data1[0x18];
	_FXGafStruct *Texture;
	char data2[0x4];
	}_Unk1Struct;//0x20

typedef struct _FXGafStruct
	{
	short	Frames;
	char	data1[6];
	char	Name[0x20];
	int		FramePointers[1];
	}_FXGafStruct;

typedef struct _DSoundStruct
	{
	char				data1[0x24];
	LPDIRECTSOUND		Directsound;
	LPDIRECTSOUNDBUFFER	DirectsoundBuffer;
	char				data2[0xC];
	LPDIRECTSOUNDBUFFER	DirectsoundBuffer2;
	}_DSoundStruct;

typedef struct _TAdynmemStruct
	{
	char				data21[0x10];
	_DSoundStruct		*DSound;
	char				data1[0x1B4F];

	_PlayerStruct		Players[10]; //starts at 0x1B63
									 //there is an extra player in data4
									 //each player is 0x14B bytes long, players[10] is 0xCEE bytes long (0xE39 if the extra "player" is counted as Players[11])

	char				data4[0x43D]; //starts at 0x2851, 0x299C if the extra "player" is put in players[10] as players[11]
									  //if this is the case, data4 is 0x2F2 bytes long

	short				BuildPosX; //0x2C8E
	short				BuildPosY;
	int					BuildPosRealX; //0x2C92
	int					Height;
	int					BuildPosRealY;
	int					unk1;
	int					Height2;

	char				data22[0x6];

	short				MouseMapPosX; //0x2CAC
	char				data16[6];
	short				MouseMapPosY; //0x2CB4
	char				data23[4];
	unsigned short		MouseOverUnit; //0x2CBA
	char				data17[0x8];
	short				BuildNum; //0x2CC4, unitindex for selected unit to build
	char				BuildSpotState; //0x40=notoktobuild
	char				data18[0x2C];

	_WeaponStruct		Weapons[256]; //0x2CF3 size=0x11500
	//char data7[4];
	int					NumProjectiles;
	_ProjectileStruct	*Projectiles; //0x141F7
	char				data13[0x10];
	_WreckageInfoStruct	*WreckageInfo; //0x1420B
	char				data14[0x24];
	int					FeatureMapSizeX; //0x14233
	int					FeatureMapSizeY; //0x14237
	char				data7[0x18];
	int					NumFeatureDefs;
	char				data15[0x18];
	_FeatureDefStruct	*FeatureDef; //0x1426F
	char				data8[8];
	LPVOID				*EyeBallMemory; //0x1427B
	char				data12[8];
	_FeatureStruct		*Features; //0x14287
	char				data3[0x40];
	tagRECT				MinimapRect;//0x142CB
	_RadarPicStruct		*RadarFinal; //0x142DB
	_RadarPicStruct		*RadarMapped; //0x142DF
	_RadarPicStruct		*RadarPicture; //0x142E3
	char				data20[4];
	short				RadarPicSizeX; //0x142EB
	short				RadarPicSizeY; //0x142ED
	char				data25[4];
	int					CirclePointer;//0x142F3 //used in drawcircle funktion
	char				data19[0x28];
	int					MapX; //0x1431f
	int					MapY; //0x14323
	int					MapXScrollingTo; //0x14327
	int					MapYScrollingTo; //0x1432B
	char				data24[0x28];
	_UnitStruct			*p_BeginUnitsArray; //0x14357
	_UnitStruct* p_EndUnitsArray;
	short				*HotUnits;//0x1435F
	short				*HotRadarUnits;
	int					NumHotUnits; //0x14367
	int					NumHotRadarUnits;
	char				data5[0x2c];
	_UnitDefStruct		*p_UnitDef; //0x1439b
	char				data11[0x57C];
	int					NumExplosions; //0x1491B
	//char data9[0x6270];
	_ExplosionStruct		Explosions[300]; //0x1491F
	LPVOID				Unk2; //0x1AB8F
	char				data10[0x1DEB4];
	int					GameTime; //0x38A47
	char				data6[0x79E];
	_MapFileStruct		*MapFile; //0x391E9
	}_TAdynmemStruct;

typedef struct _WreckageInfoStruct
	{
	int		unk1;
	LPVOID	unk2;
	int		XPos;
	int		ZPos;
	int		YPos;
	char	data1[0xC];
	short	ZTurn;
	short	XTurn;
	short	YTurn;
	char	data2[0xA];
	}_WreckageInfoStruct;

typedef struct _FeatureStruct
	{
	char	data1[8];
	short	FeatureDefIndex;
	short	WreckageInfoIndex;
	char	data2[1];
	}_FeatureStruct; //0xD

typedef struct _FeatureDefStruct
	{
	char	Name[0x20];
	char	data1[0x60];
	char	Description[20];
	char	Data2[108];
	}_FeatureDefStruct; //0x100

typedef struct _ProjectileStruct
	{
	_WeaponStruct	*Weapon;
	int				XPos;
	int				ZPos;
	int				YPos;
	int				XPosStart;
	int				ZPosStart;
	int				YPosStart;
	int				XSpeed;
	int				ZSpeed;
	int				YSpeed;
	char			data1[14];
	short			XTurn;
	short			ZTurn;
	short			YTurn;
	char			data2[45];
	struct
		{
		bool	unk1		: 1;
		bool	Inactive	: 1;
		char	unk2		: 6;
		} Inactive;
	char data3[1];
	}_ProjectileStruct; //0x6B

typedef struct _MapFileStruct
	{
	char	data[0x204];
	char	TNTFile[MAX_PATH];
	}_MapFileStruct;

typedef struct _PlayerInfoStruct
	{
	char	MapName[0x20];
	char	data1[0x76];
	char	PlayerColor;
	}_PlayerInfoStruct;

typedef struct _UnitDefStruct
	{
	char				szName[0x20];
	char				szUnitName[0x20];
	char				szUnitDescription[0x40];
	char				szObjectName[0x20];
	char				szSideinfo[0x8];

	short				nUnitID;
	BYTE				Unknown1[0x14]; //from IDA database, dont know what this is
	BYTE				AIWeights[0x40]; //AIWeights[] and AILimits[] = ascii i.e. if "3" in the fbi file the element in the array will be 0x33
	BYTE				AILimits[0x40]; //AIWeights[] and AILimits[] = ascii i.e. if "3" in the fbi file the element in the array will be 0x33
	DWORD				dwCRCFBI;
	DWORD				dwCRCAll;
	BYTE				Unknown2[4];

	short				nFootPrintX;
	short				nFootprintZ;
	DWORD				*p_YardMap;

	DWORD				dwAICanBuildCount;
	DWORD				*p_CanBuildArray;

	int					lBuildLimit;

	int					lWidthX;
	int					lWidthY; //confirm y and not z
	int					lWidthZ;

	int					lFootPrintX; //why here and also above as short?
	int					lFootPrintY; //confirm y and not z
	int					lFootPrintZ;

	int					lRelatedUnitXWidth;
	int					lRelatedUnitYWidth; //confirm y and not z
	int					lRelatedUnitZWidth;

	int					lWidthHypot;

	int					lBuildCostEnergy;
	int					lBuildCostMetal;

	DWORD				*p_COBData;

	int					lRawSpeed_MaxVelocity;	//must be multipled by the speed constant at 4FD730 (floating point) = 0.0000152587890625
												//arm commander's speed in game = 78643, for example
												//[23:31] <+Yeha> Mafia: i think you have a current x and y speed for each unit
												//so i gota find these as well . . .
	int					lRawSpeed_SlopeSpeed;

	int					lBrakeRate;
	int					lAcceleration;
	int					lBankScale;
	int					lPitchScale;
	int					lDamageModifier; //float?
	int					lMoveRate1;
	int					lMoveRate2;
	DWORD				dwMovementClass;
	short				nTurnRate;
	short				nCorpse; //ID???
	short				nMaxWaterDepth;
	short				nMinWaterDepth;

	int					lEnergyMake;
	int					lEnergyUse;
	int					lMetalMake;
	int					lExtractsMetal;
	int					lWindGenerator;
	int					lTidalGenerator;
	int					lCloakCost;
	int					lCloakCostMoving;
	int					lEnergyStorage;
	int					lMetalStorage;
	int					lBuildTime;

	DWORD				dwWeapon1; //ptr?
	DWORD				dwWeapon2;
	DWORD				dwWeapon3;

	int					nMaxDamage;

	short				nWorkerTime;
	short				nHealTime;
	short				nSightDistance;
	short				nRadarDistance;
	short				nSonarDistance;
	short				nMinCloakDistance;
	short				nRadarDistanceJam;
	short				nSonarDistanceJam;
	short				nSoundClassIndex;
	short				nBuildAngle;
	short				nBuildDistance;
	short				nManeuverLeashLength;
	short				nAttackRunLength;
	short				nKamikazeDistance;
	short				nSortBias;
	short				nCruiseAlt; //should this just be an unsigned char??
	short				nUnitTypeID;
	_WeaponStruct		*p_ExplodeAsWeapon; //confirm these are down here instead of earlier in the struct
	_WeaponStruct		*p_SelfeDestructAsWeapon; //confirm these are down here instead of earlier in the struct
	unsigned char		ucMaxSlope;
	unsigned char		ucMaxWaterSlope;
	unsigned char		ucTransportSize;
	unsigned char		ucTransportCapacity;
	unsigned char		ucWaterline;
	unsigned char		ucMakesMetal; //true/false maybe??
	unsigned char		ucGUINum;
	unsigned char		ucBMCode;
	unsigned char		ucDefaultMissionType;
	DWORD				*p_WeaponPrimaryBadTargetCategory_MaskAryPtr;
	DWORD				*p_WeaponSecondaryBadTargetCategory_MaskAryPtr;
	DWORD				*p_WeaponTertiaryBadTargetCategory_MaskAryPtr;
	DWORD				*p_NoChaseCategory_MaskAryPtr;

	struct
		{
		unsigned char StandingMoveOrder : 2;
		unsigned char StandingFireOrder : 2;
		bool Init_Cloaked : 1;
		bool Downloadable : 1;
		bool Builder : 1;
		bool ZBuffer : 1;
		bool Stealth : 1;
		bool IsAirbase : 1;
		bool IsTargetingUpgrade : 1;
		bool CanFly : 1;
		bool CanHover : 1;
		bool Teleporter : 1;
		bool HideDamage : 1;
		bool ShootMe : 1;
		BYTE Unknown1 : 1;
		bool ArmoredState : 1;
		bool ActivateWhenBuilt : 1;
		bool Floater : 1;
		bool Upright : 1;
		bool Amphibious : 1;
		BYTE Unknown2 : 1;
		BYTE Unknown3 : 1;
		bool IsFeature : 1;
		bool NoShadow : 1;
		bool ImmuneToParalyzer : 1;
		bool HoverAttack : 1;
		bool Kamikaze : 1; //offset 0x42cb12 is the code
		bool AntiWeapons : 1;
		bool Digger : 1;
		BYTE Unknown4 : 1;
		}_UnitPropertyFlags1; //DWORD. formerly dwUnitTypeMask_0

	struct
		{
		bool MobileStandOrders : 1;
		bool FireStandOrders : 1;
		bool OnOffAble : 1;
		bool CanStop : 1;
		bool CanAttack : 1;
		bool CanGuard : 1;
		bool CanPatrol : 1;
		bool CanMove : 1;
		bool CanLoad : 1;
		BYTE Unknown1 : 1;
		bool CanReclamate : 1;
		bool CanResurrect : 1;
		bool CanCapture : 1;
		BYTE UnknownResurrect : 1;
			//UnknownResurrect is strange... just after cancapture gets set, there is a "test, ah, 41h" instr
			//if not zero(i.e. 0x40 or 0x1 is set::: mobilestandorders or canpatrol), then UnknownResurrect = 0, otherwise it gets set to 1
		bool CanDGun : 1;
		bool NoRestrict : 1;
		bool Wacky : 1; //is the math here right ?? is more stuff happening than just setting this bit ?? 0x42ad99
		bool ShowPlayerName : 1;
		bool Commander : 1;
		bool CantBeTransported : 1;
		unsigned char SelfDestructCountdown : 3; //the bit field overflows if type unsigned int is used... I think just 3 bits since there's an and eax, 0x7 (0x42CBEE)
		BYTE Unknown2 : 1;
		BYTE Unknown3 : 1;
		BYTE Unknown4 : 1;
		BYTE Unknown5 : 1;
		BYTE Unknown6 : 1;
		BYTE Unknown7 : 1;
		BYTE Unknown8 : 1;
		BYTE Unknown9 : 1;
		BYTE Unknown10 : 1;
		}_UnitPropertyFlags2; //DWORD. formerly dwUnitTypeMask_1
	}_UnitDefStruct; //0x249

typedef struct _IsUnitStruct
	{
	long				data1[7];
	long				lCurrentRawSpeed;
	//more unknown stuff here, don't know how much, though
	}_IsUnitStruct;

typedef struct _UnitStruct
	{
	_IsUnitStruct		*pIsUnit; //formerly int IsUnit; (is this only for mobile units, NULL if otherwise? -- double check this)
	char				data1[12];
	_WeaponStruct		*Weapon1;
	char				data2[11];
	char				Builder;
	char				data3[12];
	_WeaponStruct		*Weapon2;
	char				data4[24];
	_WeaponStruct		*Weapon3;
	char				data5[16];
	_UnitOrdersStruct	*UnitOrders; //5C
	char				UnitState;
	char				data6[3];
	unsigned short		ZTurn;
	unsigned short		XTurn;
	unsigned short		YTurn;
	int					XPos; //0x6A
	int					ZPos;
	int					YPos;
	short				XGridPos;
	short				YGridPos;
	short				XLargeGridPos;
	short				YLargeGridPos;
	char				data8[4];
	LPVOID				UnkPTR1;
	char				data15[8];
	_UnitStruct			*FirstUnit; //?
	_UnitDefStruct		*UnitType; //0x92
	_PlayerStruct		*Owner; //?
	_AlmostCOBStruct	*pAlmostCOBStruct; //0x9A, formerly LPVOID UnkPTR2... dword ptr [pAlmostCOBStruct + 8] = start of cob file... this plus 0x2C = skip cob header
	_Object3doStruct	*Object3do;
	char				data9[22];
	short				Kills;
	char				data17[50];
	_PlayerStruct		*Owner2; //?
	//char				data16[6];
	//formerly data16:
	//begin
	_UnitStruct			*pAttacker; //I think this is it!! but it only seems to work for buildings... (0xF0)
	char				data16[2];
	//end
	char				HealthPerA; //health in percent
	char				HealthPerB; //health in percent, changes slower (?)
	char				data19[2];
	unsigned char		RecentDamage; //0xFA
	unsigned char		Height;
	char				data10[8];
	int					Nanoframe;
	short				Health;
	//char				data14[6];
	//formerly data14:
	char				data14[4];
	char				cIsCloaked; //1 for no, 5 for yes
	char				data18;

	char				UnitSelected; //seems to be 0x71 (113) for selected, 0x21 (33) for not selected

	//char				data11[7];
	//formerly data11:
	char cStatus;		//looks to contain status bits?
						//WantsToCloak: 3 for no, 11 (0xB) for yes
						//NOTE:: set this to 0x43 to kill the unit!!!!! (confirmed using arm commander)
	char data11[6];
	}_UnitStruct; //0x118

typedef struct _Object3doStruct
	{
	short			NumParts;
	char			data1[2];
	int				TimeVisible;
	char			data2[4];
	_UnitStruct		*ThisUnit;
	LPVOID			*UnkPTR1;
	LPVOID			*UnkPTR2;
	char			data3[6];
	_PrimitiveStruct	*BaseObject;
	}_Object3doStruct;

typedef struct _PrimitiveInfoStruct
	{
	char	data1[28];
	char	*Name;
	}_PrimitiveInfoStruct;

typedef struct _PrimitiveStruct
	{
	_PrimitiveInfoStruct *PrimitiveInfo;
	int				XPos;
	int				ZPos;
	int				YPos;
	unsigned short	XTurn;
	unsigned short	ZTurn;
	unsigned short	YTurn;
	char			data3[18];
	struct
		{
		bool	Visible	: 1;
		bool	unk1	: 7;
		} Visible;
	char			data2[1];
	_PrimitiveStruct	*SiblingObject;
	_PrimitiveStruct	*ChildObject;
	_PrimitiveStruct	*ParrentObject;
	}_PrimitiveStruct; //0x36

typedef struct _UnitOrdersStruct
	{
	char				data1[30];
	_UnitOrdersStruct	*ThisPTR;
	//char data2[2];
	int					PosX;
	int					PosZ;
	int					PosY;
	char				data3[6];
	char				FootPrint;
	char				data4[12];
	struct
		{
		char	unk1		: 4;
		bool	ThisStart	: 1;
		char	unk2		: 3;
		} ThisStart;
	char				data5[6];
	_UnitOrdersStruct	*NextOrder;
	}_UnitOrdersStruct; //0x4C (76)

typedef struct _GAFAnimStruct
	{

	}_GAFAnimStruct;

typedef struct _RadarPicStruct
	{
	int		XSize;
	int		YSize;
	int		Unk1;
	LPVOID	*PixelPTR;
	}_RadarPicStruct;

typedef struct _ParticleSystemStruct
	{
	LPVOID			DrawFunc; //4FD5F8 wake or smoke?; 4FD638 - Smoke1, 4FD618 - Smoke2; 4FD5B8, 4FD5A8 - Nanolath; 4FD5D8 - fire; //?
	char			data1[8];
	int				Type; //1 smoke, 2 wake, 6 nano, 7 fire
	_SmokeGraphicsStruct*	firstDraw; //0 om denna partikel ej �r aktiv (?)
	_SmokeGraphicsStruct*	lastDraw; //rita alla fram till men inte denna ?
	LPVOID			*Unk; //? inte f�r sista ?
	char			data2[48];
	}_ParticleSystemStruct;//76

typedef struct _ParticleBaseStruct
	{
	char					data[8];
	_ParticleSystemStruct	**particles; 
	char					data2[8];
	_ParticleSystemStruct	**ParticlePTRArray;
	int						SmokeParticleStructSize; //? 76
	int						maxParticles; //? 1000
	int						curParticles; //antalet aktiva i arrayen men de �r inte n�dv�ndigtvis i ordning
	}_ParticleBaseStruct;

typedef struct _SmokeGraphicsStruct
	{
	_FXGafStruct		*gaf;
	int				XPos,ZPos,YPos;
	int				unknown;
	int				frame;
	int				unknown2;
	int				MoreSubs; //0 ifall inga fler subparticles efter denna
	}_SmokeGraphicsStruct;//0x20

/*struct SmokeListNode{
SmokeParticleStruct* next;
SmokeParticleStruct* me;
};*///?

/*
struct COBStackStruct
	{
	long	lIndex; //dword ptr [esi + 8]
	char	data1[0x1C]; //0x1C = 28 decimal
	long	lStack[]; //dword ptr [esi + 24h]
	};
*/

typedef struct _AlmostCOBStruct
	{
	char data[8];
	long *pCOB;
	}_AlmostCOBStruct;


#pragma pack()